import os
import dotenv
basedir = os.path.abspath(os.path.dirname(__file__))
dotenvPath = os.path.join(os.path.dirname(__file__), '.env')
dotenv.load_dotenv(dotenvPath)


class Config(object):
    SECRET_KEY = os.environ.get('SECRET_KEY') or \
        'you-will-never-guess'
    SQLALCHEMY_DATABASE_URI = os.environ.get('DATABASE_URL') or \
        'sqlite:///' + os.path.join(basedir, 'app.db')
    SQLALCHEMY_TRACK_MODIFICATIONS = False
