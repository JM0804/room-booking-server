from flask import jsonify, request
from app import app, models, db


@app.route('/')
def index():
    return '/', 200


@app.route('/api')
def api():
    return 'API', 200


@app.route('/api/rooms')
def rooms():
    rooms_query = models.Room.query.all()
    rooms = []
    for room in rooms_query:
        returnRoom = room.as_dict()
        returnRoom['building_long'] = getLongName(returnRoom['building'])
        rooms.append(returnRoom)
    return jsonify(rooms), 200


@app.route('/api/room/<name>')
def room(name=''):
    room = models.Room.query.filter_by(name=name).first()
    if room:
        returnRoom = room.as_dict()
        returnRoom['building_long'] = getLongName(returnRoom['building'])
        print(returnRoom)
        return jsonify(returnRoom), 200
    else:
        return jsonify(False), 404


@app.route('/api/buildings')
def buildings():
    buildings_query = models.Building.query.all()
    buildings = []
    for building in buildings_query:
        buildings.append(building.as_dict())
    return jsonify(buildings), 200


@app.route('/api/building/<name>')
def building(name=''):
    building = models.Building.query.filter_by(name_short=name).first()
    if building:
        return jsonify(building.as_dict()), 200
    else:
        return jsonify(False), 404


@app.route('/api/book_room', methods=['POST'])
def book_room():
    roomID = request.form['room']
    userID = request.form['user']
    date = request.form['date']
    time = request.form['time']
    room = models.Room.query.filter_by(name=roomID).first()
    if room:
        isBooked = models.Booking.query.filter_by(
            room=roomID,
            date=date,
            time=time
        ).first()

        if isBooked:
            return jsonify(False), 404
        else:
            booking = models.Booking()
            booking.room = roomID
            booking.user_id = userID
            booking.date = date
            booking.time = time
            db.session.add(booking)
            db.session.commit()
            return jsonify(booking.id), 200
    else:
        return jsonify(False), 404


@app.route('/api/cancel_booking', methods=['POST'])
def cancel_booking():
    bookingID = request.form['id']
    booking = models.Booking.query.filter_by(id=bookingID).first()

    if booking:
        db.session.delete(booking)
        db.session.commit()
        return jsonify(True), 200
    else:
        return jsonify(False), 404


@app.route('/api/bookings', methods=['POST'])
def bookings():
    userID = request.form['user']
    bookings_query = models.Booking.query.filter_by(user_id=userID).all()
    bookings = []
    for booking in bookings_query:
        bookings.append(booking.as_dict())
    return jsonify(bookings), 200


# TESTING ONLY
#@app.route('/api/users')
#def users():
#    users_query = models.User.query.all()
#    users = []
#    for user in users_query:
#        users.append(user.as_dict())
#    return jsonify(users)

@app.errorhandler(404)
def error_404(e):
    return 'This route does not exist {}'.format(request.url), 404


def getLongName(short_name):
    return models.Building.query.filter_by(name_short=short_name)\
        .first().as_dict()['name_long']
