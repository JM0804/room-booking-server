from app import db


class User(db.Model):
    id = db.Column(db.Integer, primary_key=True)

    def __repr__(self):
        return '<User {}>'.format(self.id)
    
    def as_dict(self):
        return {c.name: getattr(self, c.name) for c in self.__table__.columns}


class Building(db.Model):
    name_short = db.Column(db.String, primary_key=True)
    name_long = db.Column(db.String)

    def __repr__(self):
        return '<Building {}>'.format(self.name_short)
    
    def as_dict(self):
        return {c.name: getattr(self, c.name) for c in self.__table__.columns}


class Room(db.Model):
    name = db.Column(db.String, primary_key=True)
    capacity = db.Column(db.Integer)
    computers = db.Column(db.Integer)
    has_projector = db.Column(db.Boolean)
    building = db.Column(db.String, db.ForeignKey('building.name_short'))
    lat = db.Column(db.Float)
    lon = db.Column(db.Float)
    type = db.Column(db.String)

    def __repr__(self):
        return '<Room {}>'.format(self.name)

    def as_dict(self):
        return {c.name: getattr(self, c.name) for c in self.__table__.columns}


class Booking(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    room = db.Column(db.String, db.ForeignKey('room.name'))
    date = db.Column(db.Date)
    time = db.Column(db.Integer)

    def __repr__(self):
        return '<Booking {}>'.format(self.id)
    
    def as_dict(self):
        return {c.name: getattr(self, c.name) for c in self.__table__.columns}
